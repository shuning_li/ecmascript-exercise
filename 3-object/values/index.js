export default function countTypesNumber(source) {
  return Object.keys(source).reduce((sum, v) => sum + Number(source[v]), 0);
}
