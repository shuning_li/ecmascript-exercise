export default function find00OldPerson(collection) {
  return collection.find(v => v.age < 20 && v.age > 9).name;
}
