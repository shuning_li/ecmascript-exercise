export default function numberMapToWord(collection) {
  return collection.map(v => String.fromCharCode(v + 96));
}
