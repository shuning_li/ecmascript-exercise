export default function collectCarNumberCount(collection) {
  return collection.filter(v => v.startsWith('粤A')).length;
}
