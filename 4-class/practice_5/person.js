// TODO 18: 在这里写实现代码
export default class Person {
  constructor(name, age) {
    this.name = name;
    this.age = age;
  }

  introduce(extra) {
    let msg = `My name is ${this.name}. I am ${this.age} years old.`;
    msg = extra ? msg.concat(' ').concat(extra) : msg;
    return msg;
  }
}
