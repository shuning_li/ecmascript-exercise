// TODO 20: 在这里写实现代码
import Person from './person';

export default class Teacher extends Person {
  constructor(name, age, klass) {
    super(name, age);
    this.klass = klass;
  }

  introduce() {
    let extra = 'I am a Teacher. I teach';
    extra = this.klass ? extra.concat(` Class ${this.klass}.`) : extra.concat(' No Class.');
    return super.introduce(extra);
  }
}
